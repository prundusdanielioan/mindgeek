<?php


namespace App\Services;

use mysql_xdevapi\Exception;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpKernel\HttpCache\Store;
use Symfony\Component\HttpClient\CachingHttpClient;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


/**
 * Class ReadJson
 * @package App\Services
 */
class ReadJson
{

    /**
     * @var string
     */
    private $storageFolder;
    /**
     * @var string
     */
    private $jsonURl;
    /**
     * @var string
     */
    private $jsonResponse;

    /**
     * @var array
     */
    private $jsonArray;

    private $cacheTime;


    /**
     * @param array $params
     * @return array
     * @throws \ErrorException
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function getJsonData( array $params)
    {
        $this->setStorageFolder($params['projectDir']);
        $this->setCacheTime($params['cacheTime']);
        $this->setJsonURl($params['jsonUrl']);
        $this->readUrl();
//        $this->saveImagesToCache();
        return $this->getJsonResponse();
    }

    /**
     * @param string $id
     * @return array
     */
    public function getDetailsById(string $id)
    {
        try {
            return $this->getJsonArray()[$id];
        }
        catch (\Exception $exception){
            throw new NotFoundHttpException("Undefined Id");
        }

    }

    private function saveImagesToCache()
    {
        $store = new Store($this->getStorageFolder() . '/var/cache/storage');
        $httpClient = HttpClient::create();
        $httpClient = new CachingHttpClient($httpClient, $store, ['default_ttl' => $this->getCacheTime()]);

        foreach ($this->getJsonArray() as $val) {
            foreach ($val['cardImages'] as $images) {
                try {
                    $response = $httpClient->request('GET', $images['url']);
                } catch (\Exception $exception) {
                }

            }
        }
    }
  public function getImagesFromCacheById( string $id){
      $store = new Store($this->getStorageFolder() . '/var/cache/storage');
      $httpClient = HttpClient::create();
      $httpClient = new CachingHttpClient($httpClient, $store, ['default_ttl' => $this->getCacheTime()]);
      $data = $this->getDetailsById($id);
      $images = [];
      foreach ($data['cardImages'] as $images){
          try {
              $response = $httpClient->request('GET', $images['url']);
              if ($response->getStatusCode() == 200){
                  $images[] = "data:image/png;base64," . base64_encode($response->getContent());

              }
          } catch (\Exception $exception) {
          }
        return $images;
      }

  }
    /**
     * @throws \ErrorException
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    private function readUrl()
    {
        try {
            $store = new Store($this->getStorageFolder() . '/var/cache/storage');
            $httpClient = HttpClient::create();
            $httpClient = new CachingHttpClient($httpClient, $store, ['default_ttl' => $this->getCacheTime()]);
            $response = $httpClient->request('GET', $this->getJsonURl());
            if ($response->getStatusCode() != 200) {
                throw  new \ErrorException('http code != 200');
            }
            $this->setJsonResponse($response->getContent());
            $this->setJsonArray($this->getJsonResponse());
//         $this->getDetailsById('1');
        } catch (Exception $exception) {
            throw  new \ErrorException($exception->getMessage());
        }
    }


    /**
     * @return mixed
     */
    public function getStorageFolder()
    {
        return $this->storageFolder;
    }

    /**
     * @param mixed $storageFolder
     */
    public function setStorageFolder($storageFolder): void
    {
        $this->storageFolder = $storageFolder;
    }

    /**
     * @return mixed
     */
    public function getJsonURl()
    {
        return $this->jsonURl;
    }

    /**
     * @param mixed $jsonURl
     */
    public function setJsonURl($jsonURl): void
    {
        $this->jsonURl = $jsonURl;
    }


    /**
     * @return string
     */
    public function getJsonResponse()
    {
        return $this->jsonResponse;
    }

    /**
     * @param mixed $jsonResponse
     */
    public function setJsonResponse($jsonResponse): void
    {
        $this->jsonResponse = utf8_encode($jsonResponse);
    }

    /**
     * @return array
     */
    public function getJsonArray(): array
    {
        return $this->jsonArray;
    }

    public function setJsonArray(): void
    {
        $data = json_decode($this->getJsonResponse(), true);

        foreach ($data as $key => $val) {

            $data[$val['id']] = $val;
            unset($data[$key]);
        }
        $this->jsonArray = $data;
    }

    /**
     * @return mixed
     */
    public function getCacheTime()
    {
        return $this->cacheTime;
    }

    /**
     * @param mixed $cacheTime
     */
    public function setCacheTime($cacheTime): void
    {
        $this->cacheTime = $cacheTime;
    }


}