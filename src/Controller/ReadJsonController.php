<?php

namespace App\Controller;

use App\Services\ReadJson;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
class ReadJsonController extends AbstractController
{
    /**
     * @Route("/read/json", name="read_json")
     * @param ReadJson $readJson
     * @return Response
     * @throws \ErrorException
     */
    public function index(ReadJson $readJson)
    {

        $params = [
          'projectDir' => $this->getParameter('app.project.dir'),
          'jsonUrl' => $this->getParameter('app.json.url'),
          'cacheTime' => $this->getParameter('app.cache.time'),
        ];
        $readJson->getJsonData( $params);
        return $this->render('read_json/index.html.twig', [
            'controller_name' => 'ReadJsonController',
            'json_data' =>  $readJson->getJsonArray(),
        ]);
    }
    /**
     * @Route("/details/json/{id}", methods={"GET"}, name="details_json")
     */
    public function details( Request $request, ReadJson  $readJson)
    {
        $params = [
            'projectDir' => $this->getParameter('app.project.dir'),
            'jsonUrl' => $this->getParameter('app.json.url'),
            'cacheTime' => $this->getParameter('app.cache.time'),
        ];


        $readJson->getJsonData( $params);
        return $this->render('read_json/details.html.twig', [
            'id' => $request->get('id' ),
            'details' => $readJson->getDetailsById($request->get('id' )),
           // 'image' => $readJson->getImagesFromCacheById($request->get('id' )),
            'image' => $readJson->getImagesFromCacheById($request->get('id' ))[0],
        ]);
    }
}
