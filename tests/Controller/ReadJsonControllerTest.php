<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Tests\Controller;

use App\Entity\Post;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Functional test for the controllers defined inside BlogController.
 *
 * See https://symfony.com/doc/current/book/testing.html#functional-tests
 *
 * Execute the application tests using this command (requires PHPUnit to be installed):
 *
 *     $ cd your-symfony-project/
 *     $ ./vendor/bin/phpunit
 */
class ReadJsonControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $client->request('GET', '/ro/read/json');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

    }
  public function testIndexLi()
    {
        $client = static::createClient();

        $client->request('GET', '/ro/read/json');

        $this->assertContains(
            '<li>',
            $client->getResponse()->getContent()
        );

    }

    public function testWrongDetails()
    {
        $client = static::createClient();

        $client->request('GET', '/ro/details/json/wrongId');

        $this->assertEquals(404, $client->getResponse()->getStatusCode());

    }

    public function testDetails()
    {
        $client = static::createClient();

       $crawler = $client->request('GET', '/ro/read/json');
        $link = $crawler->filter('a') // find all links with the text "Greet"
    ->eq(1) // select the second link in the list
    ->link();
        $crawler = $client->click($link);
        $this->assertContains(
            '<img',
            $client->getResponse()->getContent()
        );

    }


}
